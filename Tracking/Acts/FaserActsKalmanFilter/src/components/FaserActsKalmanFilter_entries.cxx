/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#include "FaserActsKalmanFilter/FaserActsKalmanFilterAlg.h"
#include "FaserActsKalmanFilter/CombinatorialKalmanFilterAlg.h"
#include "FaserActsKalmanFilter/TruthBasedInitialParameterTool.h"
#include "FaserActsKalmanFilter/SPSeedBasedInitialParameterTool.h"
#include "FaserActsKalmanFilter/SPSimpleInitialParameterTool.h"
#include "FaserActsKalmanFilter/TrajectoryWriterTool.h"
#include "FaserActsKalmanFilter/SimWriterTool.h"


DECLARE_COMPONENT(FaserActsKalmanFilterAlg)
DECLARE_COMPONENT(CombinatorialKalmanFilterAlg)
DECLARE_COMPONENT(TruthBasedInitialParameterTool)
DECLARE_COMPONENT(SPSeedBasedInitialParameterTool)
DECLARE_COMPONENT(SPSimpleInitialParameterTool)
DECLARE_COMPONENT(TrajectoryWriterTool)
DECLARE_COMPONENT(SimWriterTool)
