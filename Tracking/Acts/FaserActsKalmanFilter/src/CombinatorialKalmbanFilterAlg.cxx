#include "FaserActsKalmanFilter/CombinatorialKalmanFilterAlg.h"

#include "StoreGate/ReadHandle.h"
#include "StoreGate/ReadCondHandleKey.h"
#include "TrackerSpacePoint/FaserSCT_SpacePointCollection.h"
#include "TrackerSpacePoint/FaserSCT_SpacePoint.h"
#include "TrackerIdentifier/FaserSCT_ID.h"
#include "TrkPrepRawData/PrepRawData.h"
#include "TrackerPrepRawData/FaserSCT_Cluster.h"
#include "TrackerRIO_OnTrack/FaserSCT_ClusterOnTrack.h"
#include "TrkRIO_OnTrack/RIO_OnTrack.h"
#include "TrkSurfaces/Surface.h"
#include "Identifier/Identifier.h"
#include "Acts/Geometry/GeometryIdentifier.hpp"
#include "Acts/EventData/TrackParameters.hpp"
#include "FaserActsKalmanFilter/IndexSourceLink.h"
#include "FaserActsKalmanFilter/Measurement.h"
#include "FaserActsKalmanFilter/FaserActsRecMultiTrajectory.h"
#include "Acts/Surfaces/PerigeeSurface.hpp"
#include "Acts/MagneticField/MagneticFieldContext.hpp"

using IdentifierMap = std::map<Identifier, Acts::GeometryIdentifier>;
using ThisMeasurement = Acts::Measurement<IndexSourceLink, Acts::BoundIndices, 2>;
using TrajectoriesContainer = std::vector<FaserActsRecMultiTrajectory>;
std::array<Acts::BoundIndices, 2> indices = {Acts::eBoundLoc0, Acts::eBoundLoc1};


CombinatorialKalmanFilterAlg::CombinatorialKalmanFilterAlg(
    const std::string& name, ISvcLocator* pSvcLocator)
    : AthReentrantAlgorithm(name, pSvcLocator) {}


StatusCode CombinatorialKalmanFilterAlg::initialize() {
  ATH_MSG_INFO("CombinatorialKalmanFilterAlg::initialize");
  m_nevents=0;
  m_ntracks=0;
  m_nseeds=0;
  m_nsp10=0;
  m_nsp11=0;
  m_ncom=0;
  m_nsp1=0;
  m_nsp2=0;
  m_nsp3=0;

  ATH_CHECK(m_trackingGeometryTool.retrieve());
  ATH_CHECK(m_initialParameterTool.retrieve());
  ATH_CHECK(m_trajectoryWriterTool.retrieve());
  ATH_CHECK(detStore()->retrieve(m_idHelper,"FaserSCT_ID"));

  ATH_CHECK( m_fieldCondObjInputKey.initialize() );

  ATH_CHECK(m_trackCollection.initialize()); 

  if (m_SpacePointContainerKey.key().empty()) {
    ATH_MSG_FATAL("empty space point container key");
    return StatusCode::FAILURE;
  }
  ATH_CHECK(m_SpacePointContainerKey.initialize());
  ATH_CHECK(m_Sct_clcontainerKey.initialize() );

  return StatusCode::SUCCESS;
}


StatusCode CombinatorialKalmanFilterAlg::execute(const EventContext& ctx) const {
  ATH_MSG_DEBUG("CombinatorialKalmanFilterAlg::execute");
  m_nevents++;

  SG::ReadHandle<FaserSCT_SpacePointContainer> spcontainer(m_SpacePointContainerKey, ctx);
  if (!spcontainer.isValid()) {
    ATH_MSG_FATAL( "Could not find the data object "<< spcontainer.name());
    return StatusCode::FAILURE;
  }
  SG::ReadHandle<Tracker::FaserSCT_ClusterContainer> sct_clcontainer( m_Sct_clcontainerKey, ctx );
if (!sct_clcontainer.isValid()){
msg(MSG:: FATAL) << "Could not find the data object "<< sct_clcontainer.name() << " !" << endmsg;
return StatusCode::RECOVERABLE;
}


   //make TrackCollection                                                  
     SG::WriteHandle<TrackCollection> trackContainer{m_trackCollection,ctx};
    std::unique_ptr<TrackCollection> outputTracks = std::make_unique<TrackCollection>();

  const std::shared_ptr<IdentifierMap> identifierMap
      = m_trackingGeometryTool->getIdentifierMap();

      /*
  // Create measurement and source link containers
  IndexSourceLinkContainer sourceLinks;
  MeasurementContainer measurements;
  std::vector<Identifier> sp_ids;
  std::vector<Tracker::FaserSCT_Cluster> sps;

  std::vector<Acts::Vector3> pos1;
  std::vector<Acts::Vector3> pos2;
  std::vector<Acts::Vector3> pos3;
  pos1.clear();pos2.clear();pos3.clear();
  Tracker::FaserSCT_ClusterContainer::const_iterator coll_it = sct_clcontainer->begin();
  Tracker::FaserSCT_ClusterContainer::const_iterator coll_itend = sct_clcontainer->end();
  for (; coll_it != coll_itend; ++coll_it) {
    const Tracker::FaserSCT_ClusterCollection* spcollection = *coll_it;
    Tracker::FaserSCT_ClusterCollection::const_iterator sp_it = spcollection->begin();
    Tracker::FaserSCT_ClusterCollection::const_iterator sp_end = spcollection->end();
    for (; sp_it != sp_end; ++sp_it) {
      const Tracker::FaserSCT_Cluster* sp = *sp_it;
      Identifier id = sp->detectorElement()->identify();
      //Identifier id = sp->associatedSurface().associatedDetectorElementIdentifier();
      Acts::GeometryIdentifier geoId = identifierMap->at(id);
      IndexSourceLink sourceLink(geoId, measurements.size());
      sourceLinks.emplace_hint(sourceLinks.end(), std::move(sourceLink));
      ThisMeasurement meas(sourceLink, indices, sp->localPosition(), sp->localCovariance());
      //ThisMeasurement meas(sourceLink, indices, sp->localParameters(), sp->localCovariance());
      measurements.emplace_back(std::move(meas));

      if(m_idHelper->station(sp->identify())==1)pos1.push_back(Acts::Vector3(sp->globalPosition().x(),sp->globalPosition().y(),sp->globalPosition().z()));
      if(m_idHelper->station(sp->identify())==2)pos2.push_back(Acts::Vector3(sp->globalPosition().x(),sp->globalPosition().y(),sp->globalPosition().z()));
      if(m_idHelper->station(sp->identify())==3)pos3.push_back(Acts::Vector3(sp->globalPosition().x(),sp->globalPosition().y(),sp->globalPosition().z()));
      sp_ids.push_back(sp->identify());
      sps.push_back(*sp);
      }
      }
*/
  // Create measurement and source link containers
  IndexSourceLinkContainer sourceLinks;
  MeasurementContainer measurements;
  std::vector<Identifier> sp_ids;
  std::vector<Tracker::FaserSCT_SpacePoint> sps;

  std::vector<Acts::Vector3> pos1;
  std::vector<Acts::Vector3> pos2;
  std::vector<Acts::Vector3> pos3;
  pos1.clear();pos2.clear();pos3.clear();
  std::vector<int> layer1;
  layer1.clear();
  FaserSCT_SpacePointContainer::const_iterator coll_it = spcontainer->begin();
  FaserSCT_SpacePointContainer::const_iterator coll_itend = spcontainer->end();
  for (; coll_it != coll_itend; ++coll_it) {
    const FaserSCT_SpacePointCollection* spcollection = *coll_it;
    FaserSCT_SpacePointCollection::const_iterator sp_it = spcollection->begin();
    FaserSCT_SpacePointCollection::const_iterator sp_end = spcollection->end();
    for (; sp_it != sp_end; ++sp_it) {
      const Tracker::FaserSCT_SpacePoint* sp = *sp_it;
      Identifier id = sp->associatedSurface().associatedDetectorElementIdentifier();
      Acts::GeometryIdentifier geoId = identifierMap->at(id);
      IndexSourceLink sourceLink(geoId, measurements.size());
      sourceLinks.emplace_hint(sourceLinks.end(), std::move(sourceLink));
      ThisMeasurement meas(sourceLink, indices, sp->localParameters(), sp->localCovariance());
      measurements.emplace_back(std::move(meas));

      if(m_idHelper->station(sp->clusterList().first->identify())==1){
	pos1.push_back(Acts::Vector3(sp->globalPosition().x(),sp->globalPosition().y(),sp->globalPosition().z()));
	layer1.push_back(m_idHelper->layer(sp->clusterList().first->identify()));
      }
      if(m_idHelper->station(sp->clusterList().first->identify())==2)pos2.push_back(Acts::Vector3(sp->globalPosition().x(),sp->globalPosition().y(),sp->globalPosition().z()));
      if(m_idHelper->station(sp->clusterList().first->identify())==3)pos3.push_back(Acts::Vector3(sp->globalPosition().x(),sp->globalPosition().y(),sp->globalPosition().z()));
      sp_ids.push_back(sp->clusterList().first->identify());
      sps.push_back(*sp);
      }
      }

      // Get initial parameters
      // FIXME: Get initial parameters from clusterFitter or SeedFinder not MC!
      //  std::vector<Acts::CurvilinearTrackParameters> initialParameters;
      //  auto initialParameter = m_initialParameterTool->getInitialParameters(sp_ids);
      //  initialParameters.push_back(initialParameter);
      //std::cout<<"n spacepoints in stations : "<<pos1.size()<<" "<<pos2.size()<<" "<<pos3.size()<<std::endl;
//      if(pos1.size()<1||pos2.size()<1||pos3.size()<1) return StatusCode::SUCCESS;
      std::vector<Acts::CurvilinearTrackParameters> initialParameters;
      initialParameters.clear();
      Acts::Vector3 pos1a(0,0,0);
      Acts::Vector3 pos2a(0,0,0);
      Acts::Vector3 pos3a(0,0,0);
      for(long unsigned int i1=0;i1<pos1.size();i1++)pos1a+=pos1[i1];
      for(long unsigned int i1=0;i1<pos2.size();i1++)pos2a+=pos2[i1];
      for(long unsigned int i1=0;i1<pos3.size();i1++)pos3a+=pos3[i1];
      pos1a/=pos1.size();
      pos2a/=pos2.size();
      pos3a/=pos3.size();
      m_nsp1+=pos1.size();
      m_nsp2+=pos2.size();
      m_nsp3+=pos3.size();
//      for(int i1=0;i1<pos1.size();i1++){
//      for(int i2=0;i2<pos2.size();i2++){
//      for(int i3=0;i3<pos3.size();i3++){
//      auto initialParameter=m_initialParameterTool->getInitialParameters(pos1[i1],pos2[i2],pos3[i3]);
//      initialParameters.push_back(initialParameter);
//      }
//      }
//      }
      /*
      if(pos1.size()>0&&pos2.size()>0&&pos3.size()>0) {
      auto initialParameter=m_initialParameterTool->getInitialParameters(pos1a,pos2a,pos3a);
       if(initialParameter.momentum().z()>0){
	  m_nseeds++;
      initialParameters.push_back(initialParameter);
      */
 // for one stations
      if(pos1.size()>2) {
      std::vector<Acts::Vector3> pos10;
      std::vector<Acts::Vector3> pos11;
      std::vector<Acts::Vector3> pos12;
      pos10.clear();pos11.clear();pos12.clear();
	for(std::size_t ipos=0;ipos<pos1.size();ipos++){
	  if(layer1[ipos]==0)pos10.push_back(pos1[ipos]);
	  if(layer1[ipos]==1)pos11.push_back(pos1[ipos]);
	  if(layer1[ipos]==2)pos12.push_back(pos1[ipos]);
	}
	if(pos10.size()>0&&pos11.size()>0&&pos12.size()>0){
      auto initialParameter=m_initialParameterTool->getInitialParameters_1station(pos10,pos11,pos12);
      initialParameters.insert(initialParameters.end(),initialParameter.begin(),initialParameter.end());
      /*
       //for two stations
      if(pos1.size()>1&&pos2.size()>0) {
      Acts::Vector3 pos10a(0,0,0);
      Acts::Vector3 pos11a(0,0,0);
      int n10a=0,n11a=0;
	for(int ipos=0;ipos<pos1.size();ipos++){
	  if(layer1[ipos]==0){pos10a+=pos1[ipos];n10a++;}
	  if(layer1[ipos]>0){pos11a+=pos1[ipos];n11a++;}
	}
	m_nsp10+=n10a;
	m_nsp11+=n11a;
	if(n10a>0&&n11a>0){
	  m_ncom++;
	  pos10a/=n10a;
	  pos11a/=n11a;
	  Acts::Vector3 dir1=(pos11a-pos10a).normalized();
      auto initialParameter=m_initialParameterTool->getInitialParameters_2stations(pos1a,pos2a,dir1);
       if(initialParameter.momentum().z()>0){
	  m_nseeds++;
      initialParameters.push_back(initialParameter);
       }
       */

  // Prepare the output data with MultiTrajectory
  TrajectoriesContainer trajectories;
  trajectories.reserve(initialParameters.size());

  // Construct a perigee surface as the target surface
  auto pSurface = Acts::Surface::makeShared<Acts::PerigeeSurface>(
      Acts::Vector3{0., 0., 0.});

  Acts::PropagatorPlainOptions pOptions;
  pOptions.maxSteps = 10000;
  /*
    Acts::DirectNavigator     navigator;
     std::unique_ptr<ActsExtrapolationDetail::VariantPropagator> varProp;
      Acts::Vector3 constantFieldVector = Acts::Vector3(0,0,0.55);
	  auto bField = std::make_shared<Acts::ConstantBField>(constantFieldVector);
        auto stepper = Acts::EigenStepper<>(std::move(bField));
	auto propagator = Acts::Propagator<decltype(stepper), Acts::DirectNavigator>(std::move(stepper),
	std::move(navigator));
	varProp = std::make_unique<VariantPropagator>(propagator);
	*/

  Acts::GeometryContext geoContext = m_trackingGeometryTool->getNominalGeometryContext().context();
  Acts::MagneticFieldContext magFieldContext = getMagneticFieldContext(ctx);
  Acts::CalibrationContext calibContext;
  double chi2Max = 15;
  size_t nMax = 10;
  Acts::MeasurementSelector::Config measurementSelectorCfg = {
    {Acts::GeometryIdentifier(), {chi2Max, nMax}},
  };
  std::unique_ptr<const Acts::Logger> logger
      = Acts::getDefaultLogger("CombinatorialKalmanFilter", Acts::Logging::VERBOSE);

  // Set the CombinatorialKalmanFilter options
  CombinatorialKalmanFilterAlg::TrackFinderOptions options(
      geoContext, magFieldContext, calibContext,
      IndexSourceLinkAccessor(), MeasurementCalibrator(measurements),
      Acts::MeasurementSelector(measurementSelectorCfg),
      //Acts::LoggerWrapper{*logger}, varProp, &(*pSurface));
      Acts::LoggerWrapper{*logger}, pOptions, &(*pSurface));

  std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry
      = m_trackingGeometryTool->trackingGeometry();

  // Get track finder function
  auto trackFinderFunction = makeTrackFinderFunction(trackingGeometry);

  // Perform the track finding for all initial parameters
  ATH_MSG_DEBUG("Invoke track finding with " << initialParameters.size()
                                          << " seeds.");
  auto results = trackFinderFunction(sourceLinks, initialParameters, options);
  // Loop over the track finding results for all initial parameters
  for (std::size_t iseed = 0; iseed < initialParameters.size(); ++iseed) {
    // The result for this seed
    auto& result = results[iseed];
    if (result.ok()) {
      // Get the track finding output object
      const auto& trackFindingOutput = result.value();
      std::unique_ptr<Trk::Track> track = makeTrack(geoContext, result,sps);
      m_ntracks++;
      if(track!=nullptr)
	outputTracks->push_back(std::move(track));
      else
	ATH_MSG_DEBUG("No Trk::Track is created" );
      // Create a Trajectories result struct
      trajectories.emplace_back(std::move(trackFindingOutput.fittedStates),
                                std::move(trackFindingOutput.lastMeasurementIndices),
                                std::move(trackFindingOutput.fittedParameters));
    } else {
      ATH_MSG_WARNING("Track finding failed for seed " << iseed << " with error"
                                                       << result.error());
      // Track finding failed. Add an empty result so the output container has
      // the same number of entries as the input.
      trajectories.push_back(FaserActsRecMultiTrajectory());
    }
  }

  m_trajectoryWriterTool->writeout(trajectories, geoContext,initialParameters);
      }
      }

  if(outputTracks->size()>0)                                              
    ATH_MSG_DEBUG("Found "<<outputTracks->size()<<" tracks");
  else
    ATH_MSG_WARNING("No track is found");

  ATH_CHECK(trackContainer.record(std::move(outputTracks)));

  return StatusCode::SUCCESS;
}


StatusCode CombinatorialKalmanFilterAlg::finalize() {
  ATH_MSG_INFO("CombinatorialKalmanFilterAlg::finalize");
  ATH_MSG_INFO("Summary info");
  ATH_MSG_INFO("In taotal, "<<m_nevents<<" events, and "<<m_nseeds<<" seeds, and "<<m_ntracks<<" tracks");
  ATH_MSG_INFO("In taotal, "<<m_nsp1<<" , "<<m_nsp2<<" , "<<m_nsp3<<" ,"<<m_nsp10<<" , "<<m_nsp11<<" , "<<m_ncom);

  return StatusCode::SUCCESS;
}


Acts::MagneticFieldContext CombinatorialKalmanFilterAlg::getMagneticFieldContext(const EventContext& ctx) const {
  SG::ReadCondHandle<FaserFieldCacheCondObj> readHandle{m_fieldCondObjInputKey, ctx};
  if (!readHandle.isValid()) {
    std::stringstream msg;
    msg << "Failed to retrieve magnetic field condition data " << m_fieldCondObjInputKey.key() << ".";
    throw std::runtime_error(msg.str());
  }
  const FaserFieldCacheCondObj* fieldCondObj{*readHandle};

  return Acts::MagneticFieldContext(fieldCondObj);
}

/*
void CombinatorialKalmanFilterAlg::makeTrack(Acts::GeometryContext& tgContext, TrajectoryiesContainer trajectories,
      const FaserSCT_SpacePointContainer*  seed_spcollection, TrackCollection* outputTracks
    ) const {
  // Loop over the trajectories
  int iTraj = 0;
  for (const auto& traj : trajectories) {

    // The trajectory entry indices and the multiTrajectory
    const auto& [trackTips, mj] = traj.trajectory();
    if (trackTips.empty()) {
      ATH_MSG_WARNING("Empty multiTrajectory.");
      continue;
    }
    // Get the entry index for the single trajectory
    auto& trackTip = trackTips.front();
    std::cout<<"trackTip = "<<trackTip<<std::endl;

    // Collect the trajectory summary info
    auto trajState =
      Acts::MultiTrajectoryHelpers::trajectoryState(mj, trackTip);
      if (traj.hasTrackParameters(trackTip))
	{
	  242     mj.visitBackwards(trackTip, [&](const auto &state) {
	      243       /// Only fill the track states with non-outlier measurement
	      244       auto typeFlags = state.typeFlags();
	      245       if (not typeFlags.test(Acts::TrackStateFlag::MeasurementFlag))
	      246       {
	      247         return true;
	      248       }
	      252       /// Get the geometry ID
	      253       auto geoID = state.referenceSurface().geometryId();
	      254       m_volumeID.push_back(geoID.volume());
	      255       m_layerID.push_back(geoID.layer());
	      256       m_moduleID.push_back(geoID.sensitive());
	      257 
	      258       // expand the local measurements into the full bound space
	      259       Acts::BoundVector meas =
	      260           state.projector().transpose() * state.calibrated();
	      261 
	      262       // extract local and global position
	      263       Acts::Vector2 local(meas[Acts::eBoundLoc0], meas[Acts::eBoundLoc1]);
	      264       Acts::Vector3 mom(1, 1, 1);
	         Acts::Vector3 global =
		   266           surface.localToGlobal(geoContext, local, mom);
		 jjjjj
	}
}

*/
std::unique_ptr<Trk::Track>
//CombinatorialKalmanFilterAlg::makeTrack(Acts::GeometryContext& tgContext, FitterResult& fitResult, std::vector<Tracker::FaserSCT_Cluster> sps
CombinatorialKalmanFilterAlg::makeTrack(Acts::GeometryContext& tgContext, FitterResult& fitResult, std::vector<Tracker::FaserSCT_SpacePoint> sps
    ) const {
  std::unique_ptr<Trk::Track> newtrack = nullptr;
  //Get the fit output object
  const auto& fitOutput = fitResult.value();
//  if (fitOutput.result) 
  if (fitOutput.fittedParameters.size()>0) {
    DataVector<const Trk::TrackStateOnSurface>* finalTrajectory=new DataVector<const Trk::TrackStateOnSurface>{};
    std::vector<std::unique_ptr<const Acts::BoundTrackParameters>> actsSmoothedParam;
    ATH_MSG_DEBUG("makeTrack : trackTip "<<fitOutput.lastMeasurementIndices.size());
    // Loop over all the output state to create track state
    fitOutput.fittedStates.visitBackwards(fitOutput.lastMeasurementIndices.front(), [&](const auto &state) {
	auto flag = state.typeFlags();
	if (state.referenceSurface().associatedDetectorElement() != nullptr) {
//	const auto* actsElement = dynamic_cast<const FaserActsDetectorElement*>(state.referenceSurface().associatedDetectorElement());
//	if (actsElement != nullptr ){
	// We need to determine the type of state 
	std::bitset<Trk::TrackStateOnSurface::NumberOfTrackStateOnSurfaceTypes> typePattern;
	const Trk::TrackParameters *parm;

	// State is a hole (no associated measurement), use predicted para     meters      
	if (flag[Acts::TrackStateFlag::HoleFlag] == true){ 
	const Acts::BoundTrackParameters actsParam(state.referenceSurface().getSharedPtr(),
	    state.predicted(),
	    state.predictedCovariance());
	parm = ConvertActsTrackParameterToATLAS(actsParam, tgContext);
	//      auto boundaryCheck = m_boundaryCheckTool->boundaryCheck(*p     arm);
	typePattern.set(Trk::TrackStateOnSurface::Hole);
	}
	// The state was tagged as an outlier, use filtered parameters
	else if (flag[Acts::TrackStateFlag::OutlierFlag] == true){
	  const Acts::BoundTrackParameters actsParam(state.referenceSurface().getSharedPtr(),
	      state.filtered(),
	      state.filteredCovariance());
	  parm = ConvertActsTrackParameterToATLAS(actsParam, tgContext);
	  typePattern.set(Trk::TrackStateOnSurface::Outlier);
	} 
	// The state is a measurement state, use smoothed parameters 
	else{
	  const Acts::BoundTrackParameters actsParam(state.referenceSurface().getSharedPtr(),
	      state.smoothed(),
	      state.smoothedCovariance());

	  actsSmoothedParam.push_back(std::make_unique<const Acts::BoundTrackParameters>(Acts::BoundTrackParameters(actsParam)));
	  //  const auto& psurface=actsParam.referenceSurface();
	  //  std::cout<<"position "<<psurface.center(tgContext)<<std::endl;
	  //  std::cout<<"position "<<psurface.associatedDetectorElement()->thickness()<<std::endl;
	  //  std::cout<<"geometry "<<psurface.geometryId().volume()<<" "<<psurface.geometryId().layer()<<" "<<psurface.geometryId().sensitive()<<std::endl;
	  Acts::Vector2 local(actsParam.parameters()[Acts::eBoundLoc0], actsParam.parameters()[Acts::eBoundLoc1]);
//	  const Acts::Vector3 dir = Acts::makeDirectionUnitFromPhiTheta(actsParam.parameters()[Acts::eBoundPhi], actsParam.parameters()[Acts::eBoundTheta]);
//	  auto pos=actsParam.position(tgContext);

	  parm = ConvertActsTrackParameterToATLAS(actsParam, tgContext);
	  typePattern.set(Trk::TrackStateOnSurface::Measurement);   
	}

	Tracker::FaserSCT_ClusterOnTrack* measState = nullptr;
	if (state.hasUncalibrated()){
	  auto sp= sps.at(state.uncalibrated().index());
	  //const Tracker::FaserSCT_Cluster* fitCluster=&sp;
	  const Tracker::FaserSCT_Cluster* fitCluster=sp.clusterList().first;
	  if(fitCluster !=nullptr){
	    measState = new Tracker::FaserSCT_ClusterOnTrack{ fitCluster, Trk::LocalParameters { Trk::DefinedParameter { fitCluster->localPosition()[0], Trk::loc1 }, Trk::DefinedParameter { fitCluster->localPosition()[1], Trk::loc2 } }, fitCluster->localCovariance(), m_idHelper->wafer_hash(fitCluster->detectorElement()->identify())};
	  }
	}
	double nDoF = state.calibratedSize();
	const Trk::FitQualityOnSurface *quality = new Trk::FitQualityOnSurface(state.chi2(), nDoF);
	const Trk::TrackStateOnSurface *perState = new Trk::TrackStateOnSurface(measState, parm, quality, nullptr, typePattern);
	// If a state was succesfully created add it to the trajectory 
	if (perState) {
	  finalTrajectory->insert(finalTrajectory->begin(), perState);

	}
	//	}
	}
	return;
    });
/*
    //    Convert the perigee state and add it to the trajectory
    const Acts::BoundTrackParameters actsPer = fitOutput.fittedParameters.value();
    const Trk::TrackParameters *per  = ConvertActsTrackParameterToATLAS(actsPer, tgContext);
    std::bitset<Trk::TrackStateOnSurface::NumberOfTrackStateOnSurfaceTypes> typePattern;
    typePattern.set(Trk::TrackStateOnSurface::Perigee);
    const Trk::TrackStateOnSurface *perState = new Trk::TrackStateOnSurface(nullptr, per, nullptr, nullptr, typePattern);
    if (perState) finalTrajectory->insert(finalTrajectory->begin(), perState);
    */

    // Create the track using the states
    const Trk::TrackInfo newInfo(Trk::TrackInfo::TrackFitter::KalmanFitter, Trk::ParticleHypothesis::muon);
//     Trk::FitQuality* q = nullptr;
    //        newInfo.setTrackFitter(Trk::TrackInfo::TrackFitter::KalmanFitter     ); //Mark the fitter as KalmanFitter
    newtrack = std::make_unique<Trk::Track>(newInfo, std::move(*finalTrajectory), nullptr); 
  }
  return newtrack;
}

const Trk::TrackParameters*
CombinatorialKalmanFilterAlg ::ConvertActsTrackParameterToATLAS(const Acts::BoundTrackParameters &actsParameter, const Acts::GeometryContext& gctx) const      {
  using namespace Acts::UnitLiterals;
  std::optional<AmgSymMatrix(5)> cov = std::nullopt;
  if (actsParameter.covariance()){
    AmgSymMatrix(5) newcov(actsParameter.covariance()->topLeftCorner<5, 5> (0, 0));
    // Convert the covariance matrix to GeV
    for(int i=0; i < newcov.rows(); i++){
      newcov(i, 4) = newcov(i, 4)*1_MeV;
    }
    for(int i=0; i < newcov.cols(); i++){
      newcov(4, i) = newcov(4, i)*1_MeV;
    }
    cov =  std::optional<AmgSymMatrix(5)>(newcov);
  }
  const Amg::Vector3D& pos=actsParameter.position(gctx);
  double tphi=actsParameter.get<Acts::eBoundPhi>();
  double ttheta=actsParameter.get<Acts::eBoundTheta>();
  double tqOverP=actsParameter.get<Acts::eBoundQOverP>()*1_MeV;
  double p = std::abs(1. / tqOverP);
  Amg::Vector3D tmom(p * std::cos(tphi) * std::sin(ttheta), p * std::sin(tphi) * std::sin(ttheta), p * std::cos(ttheta));
  const Trk::CurvilinearParameters * curv = new Trk::CurvilinearParameters(pos,tmom,tqOverP>0, cov);
  return curv;  
} 

